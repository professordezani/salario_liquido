import 'package:flutter/material.dart';

void main() {
  runApp(CalculadoraApp());
}

class CalculadoraApp extends StatefulWidget {
  @override
  State<CalculadoraApp> createState() => _CalculadoraAppState();
}

class _CalculadoraAppState extends State<CalculadoraApp> {

  var _formKey = GlobalKey<FormState>();

  double salarioBruto = 0.0;
  double salarioLiquido = 0.0;
  double ir = 0.0;
  double inss = 0.0;

  void calcular() {
    if(_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      // fazer os cálculos do salário líquido:
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Calculadora Salário"),
        ),
        body: Container(
          margin: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Salário Bruto"),
              Form(
                key: _formKey,
                child: TextFormField(
                  onSaved: (value) => salarioBruto = double.parse(value!),
                  validator: (value) {
                    if(value!.isEmpty || double.parse(value) <= 0)
                      return "Valor inválido";
                    else
                      return null;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 50),
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: calcular,
                  child: Text("Calcular"),
                ),
              ),
              Text("Salário Líquido"),
              Text("R\$ $salarioLiquido"),
              SizedBox(height: 10,),
              Row(
                children: [
                  Expanded(
                    child: Text("IR R\$ $ir"),
                  ),
                  Expanded(
                    child: Text("INSS R\$ $inss"),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 50),
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: null,
                  child: Text("Limpar"),
                ),
              ),
            ]
          ),
        ),
      ),
    );
  }  
}